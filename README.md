<h1>Шаблон для проекта.</h1>
<ul>
    <li>HTML(<a href="http://jade-lang.com/">Jade</a>)</li>
    <li>CSS(<a href="http://compass-style.org/">Compass</a>, <a href="http://sass-lang.com/">SCSS</a>)</li>
    <li>JavaScript(<a href="http://coffeescript.org/">CoffeeSript</a>, <a href="http://browserify.org/">Browserify</a>)</li>
    <li>Framework(<a href="http://foundation.zurb.com/sites/docs/">Foundation</a>)</li>
    <li>Font Icon(<a href="http://fontawesome.io/cheatsheet/">Font Awesome</a>)</li>
    <li>Package manager(<a href="http://bower.io/">Bower</a>)</li>
</ul>

<p>Сборoщик проекта Gulp (<a href="https://gitlab.com/Monocrystals/start/blob/master/gulp/config.coffee">config</a>)</p>
<p><i>Основные задачи:</i></p>
<ul>
    <li>
        <code>gulp</code> build
    </li>
    <li>
        <code>gulp</code> development (по умолчанию запускается <a href="http://localhost:8000/">локальный сервер</a>, <a href="http://localhost:3000/">json server</a>)
    </li>
</ul>
<p><i>Вспомогательные задачи:</i></p>
<ul>
    <li><code>gulp jade</code> html</li>
    <li><code>gulp compass</code> css</li>
    <li><code>gulp browserify</code> js</li>
</ul>
<p><i>Перед началом работы запустить:</i></p>
<ul>
    <li><code>npm install</code></li>
    <li><code>bower install</code></li>
</ul>

<h2>Jade</h2>
<p>Компилируемые шаблоны(страницы) находятся в <a href="https://gitlab.com/Monocrystals/start/tree/master/assets/jade/pages">assets/jade/pages</a>.</p>
<p>Даные из <a href="https://gitlab.com/Monocrystals/start/tree/master/assets/data">assets/data/db.json</a> доступны внутри шаблонов, 
    так же можно создать json файл в <a href="https://gitlab.com/Monocrystals/start/tree/master/assets/jade/data">assets/jade/data</a>, который будет доступен только на соответствующей странице (имена json файла и шаблона должны совпадать). </p>
<h2>Browserify</h2>
<p>Шаблоны Jade можно использовать как модули(require).</p>
<p>Пакеты из Bower доступны как модули, но нужно смотреть как они себя ведут при подключении.<p>