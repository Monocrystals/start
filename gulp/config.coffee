paths =
	src: 'assets'
	dest: 'public/assets'
	public: 'public'
	vendor: 'vendor/bower_components'

module.exports =
	clean:
		src: paths.public + '/**/*'
	jade:
		src: paths.src + '/jade/pages/**/*'
		watchSrc: [paths.src + '/jade/**/*', paths.src + '/data/db.json']
		dest: paths.public
		data: paths.src + '/data/db.json'
		pravateData: paths.src + '/jade/data'
		options:
			pretty: true
	compass:
		src: paths.src + '/scss/**/*.scss'
		dest: paths.dest + '/css'
		autoprefixer: ['last 4 version', '> 1%', 'ie 9']
		options:
			# import_path: paths.vendor
			css: paths.dest + '/css'
			sass: paths.src + '/scss'
			image: paths.src + '/img/icons'
			style: 'nested'
			generated_images_path: paths.dest + '/img'
	browserify:
		src: paths.src + '/coffee/*.coffee'
		watchSrc: paths.src + '/coffee/**/*'
		dest: paths.dest + '/js'
		options:
			transform: ['coffeeify', 'debowerify', 'jadeify']
			extensions: ['.coffee']
			debug: true
	img:
		src: [paths.src + '/img/**', '!' + paths.src + '/img/icons/**/*']
		dest: paths.dest + '/img'
		optimizationLevel: 7
	fonts:
		src: [paths.src + '/fonts/*', paths.vendor + '/font-awesome/fonts/*']
		dest: paths.dest + '/fonts'
	jsonServer:
		data:  paths.src + '/data/db.json'
		port: '3000'
		rewriteRules: ''
		baseUrl: ''
	webServer:
		src: paths.public
		port: '8000'
	watch: ['compass', 'browserify', 'jade', 'img', 'fonts']