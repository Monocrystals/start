gulp     = require 'gulp'
jade     = require 'gulp-jade'
data     = require 'gulp-data'
plumber  = require 'gulp-plumber'
jsonfile = require 'jsonfile'
path     = require 'path'

log      = require 'log'
config   = require('config').jade

module.exports = () ->
	gulp.src config.src
		# include common json file
		.pipe data jsonfile.readFileSync config.data
		# include json file for current file
		.pipe data (file) ->
			filePath = config.pravateData + '/' + path.basename(file.path, path.extname file.path) + path.extname config.data
			# check file
			jsonfile.readFile filePath, (err, obj) -> return obj if !err
		.pipe plumber errorHandler: log
		.pipe jade config.options
		.pipe gulp.dest config.dest