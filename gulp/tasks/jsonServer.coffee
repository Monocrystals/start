gulp       = require 'gulp'
jsonServer = require 'gulp-json-srv'
jsonfile   = require 'jsonfile'
gutil      = require 'gulp-util'

config     = require('config').jsonServer

module.exports = () ->
	server = jsonServer.start config

	gulp.watch config.data, (file) ->
		jsonfile.readFile file.path, (err, obj) ->
			if err
				gutil.beep()
				gutil.log gutil.colors.red err
			else
				server.reload()