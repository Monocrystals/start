gulp      = require 'gulp'
webServer = require 'gulp-webserver'
config    = require('config').webServer

module.exports  = () ->
	gulp.src config.src
		.pipe webServer
			livereload: true
			port: config.port