gulp       = require 'gulp'
gulpif     = require 'gulp-if'
uglify     = require 'gulp-uglify'
browserify = require 'browserify'
source     = require 'vinyl-source-stream'
buffer     = require 'vinyl-buffer'
glob       = require 'glob'
path       = require 'path'
es         = require 'event-stream'

log        = require 'log'
config     = require('config').browserify

module.exports = () ->
	files  = glob.sync config.src

	streams = files.map (file) ->
		config.options['entries'] = file
		if ENV? == 'prod'
			config.options['debug'] = false

		return browserify config.options
			.bundle()
			.on 'error', (err) ->
				log.bind(@) err
			.pipe source path.basename(file, path.extname file) + '.js'
			.pipe buffer()
			.pipe gulpif ENV? == 'prod', uglify()
			.pipe gulp.dest config.dest

	return es.merge streams