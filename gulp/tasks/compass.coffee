gulp    = require 'gulp'
compass = require 'gulp-compass'
prefix  = require 'gulp-autoprefixer'
plumber = require 'gulp-plumber'

log     = require 'log'
config  = require('config').compass

module.exports = () ->
	if ENV? == 'prod'
		config.options.style = 'compressed'

	gulp.src config.src
		.pipe plumber errorHandler: log
		.pipe compass config.options
		.pipe prefix config.autoprefixer
		.pipe gulp.dest config.dest