gulp       = require 'gulp'
sequence   = require 'gulp-sequence'
requireDir = require 'require-dir'

module.exports = () ->
	tasks = requireDir './tasks', recurse: true
	gulp.task name, task for name, task of tasks

	def = ['compass', 'browserify', 'img', 'jade', 'fonts']

	gulp.task 'default', (cb) ->
		sequence 'clean', def, ['webServer', 'jsonServer', 'watch'], cb

	gulp.task 'build', (cb) ->
		global.ENV = 'prod'
		sequence 'clean', def, cb